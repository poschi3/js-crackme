Christmas Crack Me
==================

Run the Script
--------------

1. clone this repo
2. run crackme.html in a browser of your choice
3. enter secret in prompt and see if you're right

Crack It
--------

- use browser / runtimes of your choice
- use debugger and static analysies methods
- write your own scripts to break the code
- **when you are done, i would appreciate if you let me know your proceeding** (in Gitlab Issues section)